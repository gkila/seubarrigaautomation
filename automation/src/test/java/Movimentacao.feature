#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Movimentacao

  @tag1
  Scenario Outline: Criar movimentacao tipo Receita
    Given Acesso o site  
   And Clico no botao Contas
   And Clico em Adicionar   
   When Eu preencho o campo com "<name>" valido
   And Eu Clico no botao salvar
   Then a conta é adicionada com sucesso
    When clicar no botao Criar Movimentacao
    And Alterar o tipo de movimentacao para Receita
    And preencher a data da movimentacao e de pagamentos com a data atual
    And preencho os outros campos com dados validos
    And clico no botao salvar
    Then Verifico se a mensagem de sucesso para a movimentacao foi exibida
    
     Examples: 
      | name| 
      | 1   |
      
    
    
    @tag2
  Scenario Outline: Criar movimentacao tipo Despesa
     Given Acesso o site  
   And Clico no botao Contas
   And Clico em Adicionar   
   When Eu preencho o campo com "<name>" valido
   And Eu Clico no botao salvar
   Then a conta é adicionada com sucesso  
    When clicar no botao Criar Movimentacao
    And Alterar o tipo de movimentacao para Despesa
    And preencher a data da movimentacao e de pagamentos com a data atual
    And preencho os outros campos com dados validos
    And clico no botao salvar
    Then Verifico se a mensagem de sucesso para a movimentacao foi exibida
    
     Examples: 
      | name| 
      | 1   |
   
     
   
