#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Conta

  @tag1
  Scenario Outline: Adicionar contas
   Given Acessei a home do site  
   And Clico no botao Contas
   And Clico em Adicionar   
   When Eu preencho o campo com "<name>" valido
   And Eu Clico no botao salvar
   Then a conta é adicionada com sucesso

    Examples: 
      | name| 
      | 2   |
      | 3   |
      
  
  @tag2
  Scenario: Remover uma conta
   Given Acessei a home do site  
   And Clico no botao Contas
   And Clico em Listar
   When Clico no X para excluir uma conta  
   Then a conta é excluida com sucesso
   
