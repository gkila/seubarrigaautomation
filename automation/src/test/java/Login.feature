#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Login

  @tag1
  Scenario: Criando novo usuario
    Given Acessar o site e quero criar um usuario novo    
    When clicar no botao Novo Usuario?
    And preencher os campos nome, email, e senha
    And cicar no botao Cadastrar
    Then Verifico se a mensagem de sucesso foi exibida
    When Quando eu preencho os campos email e senha com valores validos
    And clico no botao Login
    Then Estou logado com sucesso
     
   
