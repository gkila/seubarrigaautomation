package AutomationProject.automation;

import java.util.Calendar;

import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.*;

public class ResumoMensalSteps {
	ResumoMensalPage rm = new ResumoMensalPage();
	Calendar cal = Calendar.getInstance();
    int day = cal.get(Calendar.DATE);
    int month = cal.get(Calendar.MONTH) + 1;
    int year = cal.get(Calendar.YEAR); 

	

	@When("clicar no botao Resumo mensal")
	public void clicarResumoMensal() {
		HomePage hp = new HomePage();
		hp.resumoMensal.click();
	}

	@And("preencher o mes e ano vigente nos filtros")
	public void preencherFiltrosMesAnoVigente() {
		Select dropdown= new Select(rm.mes);
		dropdown.selectByValue(""+month);
		
		Select dropdown2= new Select(rm.ano);
		dropdown2.selectByValue(""+year);
	}

	@And("clicar em buscar")
	public void clicarBuscar() {
		rm.buscarBtn.click();
	}

	@Then("O sistema retorna os valores")
	public void sistemaRetorna() {
		rm.excluirMovimentacao.click();
		rm.excluirMovimentacao.click();
	}

}
