package AutomationProject.automation;


import org.openqa.selenium.support.PageFactory;

public class LoginPage extends LoginMap {

	public LoginPage() {
		PageFactory.initElements(TestRule.getDriver(), this);
	}

	public void AccessSite() {

	}

	public void clickNewUser() {

	}

	public void FillNewUserFields() {

	}

	public void clickCreateUser() {

	}

	public void verifyMsg() {

	}	

	public void FillEmailAndPassFields() {

	}

	public void clickLoginBtn() {

	}

	public void onSite() {

	}

}
