package AutomationProject.automation;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MovimentacaoMap {

	@FindBy(xpath = "//*[@id='tipo']")
	public WebElement tipoMovimentacao;
	
	@FindBy(xpath = "//*[@id='tipo']/option[1]")
	public WebElement receitaOption;	
	
	@FindBy(xpath = "//*[@id='tipo']/option[2]")
	public WebElement despesaOption;	
	
	@FindBy(xpath = "//*[@id='data_transacao']")
	public WebElement dataMovimentacao;	
	
	@FindBy(xpath = "//*[@id='data_pagamento']")
	public WebElement dataPagamento;	
	
	@FindBy(xpath = "//*[@id='descricao']")
	public WebElement descricao;	
	
	@FindBy(xpath = "//*[@id='interessado']")
	public WebElement interessado;
	
	@FindBy(xpath = "//*[@id='valor']")
	public WebElement valor;
	
	@FindBy(xpath = "//*[@id='conta']")
	public WebElement conta;	
	
	@FindBy(xpath = "//button[contains(text(),'Salvar')]")
	public WebElement salvarBtn;
	
	@FindBy(xpath = "/html/body/div[1]")
	public WebElement MsgSucessoAddMovimentacao;	
	
	
	
	
	
	
	
}
