package AutomationProject.automation;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResumoMensalMap {

	@FindBy(xpath = "//*[@id='mes']")
	public WebElement mes;	
	
	@FindBy(xpath = "//*[@id='ano']")
	public WebElement ano;	
	
	@FindBy(xpath = "//input[@value = 'Buscar']")
	public WebElement buscarBtn;
	
	@FindBy(xpath = "//*[@id='tabelaExtrato']/tbody/tr/td[6]/a/span")
	public WebElement excluirMovimentacao;	
	
}
