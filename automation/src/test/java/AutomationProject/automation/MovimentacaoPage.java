package AutomationProject.automation;

import org.openqa.selenium.support.PageFactory;

public class MovimentacaoPage extends MovimentacaoMap {

	public MovimentacaoPage() {
		PageFactory.initElements(TestRule.getDriver(), this);
	}

	public void test1() {

	}

	public void clicarCriarMovimentacao() {

	}

	public void alterarParaReceita() {

	}

	public void preencherComDataAtual() {

	}

	public void preencherOutrosCamposDadosValidos() {

	}

	public void clicarSalvar() {

	}

	public void validarMsgSucessoMovimentacao() {

	}

}
