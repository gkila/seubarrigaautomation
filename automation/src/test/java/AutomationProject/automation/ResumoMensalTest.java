package AutomationProject.automation;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/ResumoMensal.feature", format = ("json:target/ResumoMensal.json"),
glue = { "" }, monochrome = true, dryRun = false)
public class ResumoMensalTest {

}
