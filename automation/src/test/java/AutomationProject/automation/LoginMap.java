package AutomationProject.automation;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginMap {
	
	@FindBy(xpath = "//*[@id='email']")
	public WebElement emailField;
	
	@FindBy(xpath = "//*[@id='senha']")
	public WebElement passwordField;
	
	@FindBy(xpath = "//button[contains(text(),'Entrar')]")
	public WebElement loginBtn;
	
	@FindBy(xpath = "//*[@id='bs-example-navbar-collapse-1']/ul/li[2]/a")
	public WebElement newUserBtn;	
	
	@FindBy(xpath = "//*[@id='nome']")
	public WebElement newNameField;
	
	@FindBy(xpath = "//*[@id='email']")
	public WebElement newEmailField;
	
	@FindBy(xpath = "//*[@id='senha']")
	public WebElement newPasswordField;
	
	@FindBy(xpath = "/html/body/div[2]/form/input")
	public WebElement createUserBtb;
	
	@FindBy(xpath = "/html/body/div[1]")
	public WebElement successNewUserMsg;
	
	String newEmail="";
	String newPass="";
	
	

}
