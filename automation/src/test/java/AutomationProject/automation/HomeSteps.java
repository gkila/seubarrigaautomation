package AutomationProject.automation;

import static org.junit.Assert.assertEquals;

import cucumber.api.java.en.*;

public class HomeSteps {

	HomePage homepage = new HomePage();

	@Given("Acessei a home do site")
	public void acessarHome() {
		LoginPage loginPage = new LoginPage();
		loginPage.newEmailField.sendKeys("gk@gk.com");
		loginPage.newPasswordField.sendKeys("123456");		
		loginPage.loginBtn.click();
	}

	@And("Clico no botao Contas")
	public void clicarContas() {
		homepage.menuContas.click();
	}

	@And("Clico em Adicionar")
	public void clicarAdicionarConta() {
		homepage.adicionarConta.click();
	}

	@When("^Eu preencho o campo com \"([^\"]*)\" valido$")
	public void preencherCampoNome(String args) {
		homepage.campoNomeAdicionarConta.sendKeys(args);
	}

	@And("Eu Clico no botao salvar")
	public void clicarSalvarConta() {
		homepage.salvarBtn.click();
	}

	@Then("a conta é adicionada com sucesso")
	public void verificarContaAdicionada() {

	}
	////////////////////////////////////////

	@And("Clico em Listar")
	public void clicarListarConta() {
		homepage.listarContas.click();
	}

	@When("Clico no X para excluir uma conta")
	public void excluirConta() {
		homepage.btnExluirConta.click();
	}

	@Then("a conta é excluida com sucesso")
	public void validarContaExcluida() {
		assertEquals("Conta removida com sucesso!", homepage.contaRemovidaMsg.getText());
		homepage.btnExluirConta.click();
	}

}
