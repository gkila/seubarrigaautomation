package AutomationProject.automation;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomeMap {
	
	@FindBy(xpath = "//*[@id='navbar']/ul/li[2]/a")
	public WebElement menuContas;
	
	@FindBy(xpath = "//*[@id='navbar']/ul/li[2]/ul/li[1]/a")
	public WebElement adicionarConta;
	
	@FindBy(xpath = "//*[@id='nome']")
	public WebElement campoNomeAdicionarConta;
	
	@FindBy(xpath = "//button[contains(text(),'Salvar')]")
	public WebElement salvarBtn;
	
	@FindBy(xpath = "//*[@id='navbar']/ul/li[2]/ul/li[2]/a")
	public WebElement listarContas;
	
	@FindBy(xpath = "//*[@id='tabelaContas']/tbody/tr[1]/td[2]/a[2]")
	public WebElement btnExluirConta;
	
	@FindBy(xpath = "/html/body/div[1]")
	public WebElement contaRemovidaMsg;
	
	@FindBy(xpath = "//*[@id='navbar']/ul/li[3]/a")
	public WebElement btnMovimentacao;
	
	@FindBy(xpath = "//*[@id='navbar']/ul/li[4]/a")
	public WebElement resumoMensal;
	
	
	
	
}
