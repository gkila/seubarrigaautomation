package AutomationProject.automation;

import org.openqa.selenium.support.PageFactory;

public class HomePage extends HomeMap {

	public HomePage() {
		PageFactory.initElements(TestRule.getDriver(), this);
	}

	public void acessarHome() {

	}

	public void clicarContas() {

	}

	public void clicarAdicionarConta() {

	}

	public void preencherCampoNome(String args) {

	}

	public void clicarSalvarConta() {

	}

	public void verificarContaAdicionada() {

	}

	public void clicarListarConta() {

	}

	public void excluirConta() {

	}

	public void validarContaExcluida() {

	}

}
