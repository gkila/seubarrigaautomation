package AutomationProject.automation;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import cucumber.api.java.en.*;

public class MovimentacaoSteps {
	MovimentacaoPage mp = new MovimentacaoPage();
	Calendar cal = Calendar.getInstance();
    int day = cal.get(Calendar.DATE);
    int month = cal.get(Calendar.MONTH) + 1;
    int year = cal.get(Calendar.YEAR); 
	
	
	@Given ("Acesso o site")
	public void test1(){
		LoginPage loginPage = new LoginPage();
		loginPage.newEmailField.sendKeys("gk@gk.com");
		loginPage.newPasswordField.sendKeys("123456");
		loginPage.loginBtn.click();
	}
    @When ("clicar no botao Criar Movimentacao")
public void clicarCriarMovimentacao(){
		HomePage hp = new HomePage();
		hp.btnMovimentacao.click();
	}
    @And ("Alterar o tipo de movimentacao para Receita")
public void alterarParaReceita(){
		mp.tipoMovimentacao.click();
		mp.receitaOption.click();
		
	}
    @And ("preencher a data da movimentacao e de pagamentos com a data atual")
public void preencherComDataAtual(){
    	mp.dataMovimentacao.sendKeys(""+day+"/"+month+"/"+year);
    	mp.dataPagamento.sendKeys(""+day+"/"+month+"/"+year);
    	
	}
    @And ("preencho os outros campos com dados validos")
public void preencherOutrosCamposDadosValidos(){
		mp.descricao.sendKeys("teste");
		mp.interessado.sendKeys("teste teste");
		mp.valor.sendKeys("20");
	}
    @And ("clico no botao salvar")
public void clicarSalvar(){
		mp.salvarBtn.click();
	}
    @Then ("Verifico se a mensagem de sucesso para a movimentacao foi exibida")
public void validarMsgSucessoMovimentacao(){
    	assertEquals("Movimentação adicionada com sucesso!", mp.MsgSucessoAddMovimentacao.getText());
	}
    
    
    @And ("Alterar o tipo de movimentacao para Despesa")
    public void alterarParaDespesa(){
		mp.tipoMovimentacao.click();
		mp.despesaOption.click();
		
	}  
    

}
