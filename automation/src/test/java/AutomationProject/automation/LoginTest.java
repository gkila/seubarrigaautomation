package AutomationProject.automation;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/Login.feature", format = ("json:target/LoginTest.json"),
glue = { "" }, monochrome = true, dryRun = false)
public class LoginTest {

}
