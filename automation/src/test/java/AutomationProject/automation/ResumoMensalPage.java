package AutomationProject.automation;

import org.openqa.selenium.support.PageFactory;


public class ResumoMensalPage extends ResumoMensalMap {
	
	public ResumoMensalPage() {
		PageFactory.initElements(TestRule.getDriver(), this);
	}
	
	
	
	public void entrarSistema() {
		
	}

	
	public void clicarResumoMensal() {
		
	}

	
	public void preencherFiltrosMesAnoVigente() {
		
	}

	
	public void clicarBuscar() {
	
	}

	
	public void sistemaRetorna() {

	}
	
	

}
