package AutomationProject.automation;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import cucumber.api.java.en.*;

public class LoginSteps {
	
	LoginPage loginPage = new LoginPage();
	public static String email = "";
	public static String senha = "";

	@Given("Acessar o site e quero criar um usuario novo")
	public void AccessSite() {
		
	}

	@When("clicar no botao Novo Usuario?")
	public void clickNewUser() {
		loginPage.newUserBtn.click();
	}

	@And("preencher os campos nome, email, e senha")
	public void FillNewUserFields() {
		Random r = new Random();
		loginPage.newEmail = String.valueOf(r.nextInt(1000))+"@teste.com";
		loginPage.newPass = String.valueOf(r.nextInt(1000));
		loginPage.newNameField.sendKeys("teste");
		loginPage.newEmailField.sendKeys(loginPage.newEmail);
		loginPage.newPasswordField.sendKeys(loginPage.newPass);
		email =loginPage.newEmail;
		senha = loginPage.newPass;
		
	}

	@And("cicar no botao Cadastrar")
	public void clickCreateUser() {
		loginPage.createUserBtb.click();
	}

	@Then("Verifico se a mensagem de sucesso foi exibida")
	public void verifyMsg() {
		assertEquals("Usuário inserido com sucesso", loginPage.successNewUserMsg.getText());
	}	

    @When ("Quando eu preencho os campos email e senha com valores validos")
    public void FillEmailAndPassFields() {
		loginPage.emailField.sendKeys(loginPage.newEmail);
		loginPage.passwordField.sendKeys(loginPage.newPass);
	}

    @And ("clico no botao Login")
    public void clickLoginBtn() {
		loginPage.loginBtn.click();
	}

    @Then ("Estou logado com sucesso")
    public void onSite() {
		
	}
}
