package AutomationProject.automation;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/Movimentacao.feature", format = ("json:target/MovimentacaoTest.json"),
glue = { "" }, monochrome = true, dryRun = false)
public class MovimentacaoTest {

}
